/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef MANAGERMENUACTION_H
#define MANAGERMENUACTION_H

#include <QWidgetAction>
#include <QModelIndex>

class Manager;
class QTreeView;
class ManagerMenuAction : public QWidgetAction
{
    Q_OBJECT
public:
    explicit ManagerMenuAction(Manager *manager, QObject *parent = 0);
    ~ManagerMenuAction();

    QWidget *createWidget(QWidget *parent);

private Q_SLOTS:
    void createIndexWidgets(const QModelIndex &parent, int start, int end);

private:
    Manager *m_manager;
    QTreeView *m_networkView;

};

#endif // MANAGERMENUACTION_H
