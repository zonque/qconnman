/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QCoreApplication>
#include <QStringList>
#include <QDebug>

#include "vpn/vpnconnection.h"
#include "vpn/vpnmanager.h"

void printUsage(const char *appName)
{
    qDebug() << "usage: " << appName << " <command> [options]" << endl
             << "\tconnections" << endl
             << "\tremove <connection>" << endl
             << "\tconnect <connection>" << endl
             << "\tdisconnect <connection>" << endl;
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    QStringList args = app.arguments();
    if (args.length() < 2) {
        printUsage(argv[0]);
        return -1;
    }

    VpnManager manager;
    QString command = args.at(1);
    if (command == QLatin1String("connections")) {
        foreach (VpnConnection *connection, manager.connections())
            qDebug() << connection;
    } else if (command == QLatin1String("connect") ||
               command == QLatin1String("disconnect") ||
               command == QLatin1String("remove")) {
        if (args.length() < 3) {
            qDebug() << "path required";
            printUsage(argv[0]);
            return -1;
        }

        QString path = args.at(2);
        if (command == QLatin1String("remove")) {
            manager.remove(QDBusObjectPath(path));
            return 0;
        } else {
            foreach (VpnConnection *connection, manager.connections()) {
                if (connection->objectPath().path() == path) {
                    if (command == QLatin1String("connect"))
                        connection->connect();
                    else
                        connection->disconnect();
                }
            }
        }
    } else {
        qDebug() << "unknown command: " << command;
        printUsage(argv[0]);
        return -1;
    }

    return 0;
}
