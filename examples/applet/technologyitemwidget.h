#ifndef _TECHNOLOGYITEMWIDGET_H
#define _TECHNOLOGYITEMWIDGET_H

#include "ui_technologyitemwidget.h"

class Technology;
class TechnologyItemWidget: public QWidget,
                            private Ui::TechnologyItemWidget
{
    Q_OBJECT
public:
    TechnologyItemWidget(Technology *technology, QWidget *parent = 0);
    ~TechnologyItemWidget();

private Q_SLOTS:
    void updateInformation();

private:
    Technology *m_technology;

};

#endif
