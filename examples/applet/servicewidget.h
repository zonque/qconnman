/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#ifndef SERVICEWIDGET_H
#define SERVICEWIDGET_H

#include <QWidget>
#include <QAbstractButton>

#include "service.h"

namespace Ui {
class ServiceWidget;
}

class QTimeLine;
class ServiceWidget : public QAbstractButton
{
    Q_OBJECT

public:
    explicit ServiceWidget(Service *service, QWidget *parent = 0);
    ~ServiceWidget();

protected:
    virtual void paintEvent(QPaintEvent *e);

private Q_SLOTS:
    void updateServiceData();
    void updateAnimation(int frame);
    void serviceClicked();

private:
    void setupLoaderAnimation();

    Ui::ServiceWidget *ui;
    Service *m_service;
    QTimeLine *m_loaderAnimation;

};

#endif // SERVICEWIDGET_H
