/*
 * Copyright (C) 2012 Matt Broadstone
 * Contact: http://bitbucket.org/devonit/qconnman
 *
 * This file is part of the QConnman Library.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */

#include <QCoreApplication>
#include <QProcess>
#include <QUrl>
#include <QDebug>

#include "vpn/vpnconnection.h"
#include "vpn/vpnproviders.h"
#include "vpn/vpnmanager.h"

QVariantMap openConnectHelper(const QString &host, const QString &domain,
                              const QString &user, const QString &password)
{
    QStringList arguments;
    arguments << "--authenticate"
              << "--authgroup" << domain
              << "--user" << user
              << "--no-cert-check"
              << "--no-xmlpost"     // this is legacy, but required for testing on my ubuntu box
              << host;

    QProcess openConnect;
    openConnect.setReadChannel(QProcess::StandardError);
    openConnect.start("openconnect", arguments);
    if (!openConnect.waitForStarted()) {
        qDebug() << "unable to start openconnect binary";
        return QVariantMap();
    }

    bool passwordWritten = false;
    forever {
        if (!openConnect.waitForReadyRead()) {
            qDebug() << "no data";
            break;
        }

        if (openConnect.state() == QProcess::NotRunning) {
            qDebug() << "process terminated early";
            break;
        }

        QByteArray data = openConnect.readAll();
        if (!passwordWritten) {
            if (!data.contains("Password:"))
                continue;

            openConnect.setReadChannel(QProcess::StandardOutput);
            openConnect.write(password.toUtf8());
            openConnect.closeWriteChannel();
            passwordWritten = true;
        } else {
            openConnect.waitForFinished();

            QVariantMap result;
            QStringList variables = QString(data).split('\n', QString::SkipEmptyParts);
            foreach (QString variable, variables) {
                QStringList parts = variable.split('=', QString::SkipEmptyParts);
                QString value = parts.last();
                if (value.startsWith("'")) value.remove(0, 1);
                if (value.endsWith("'")) value.remove(value.size() - 1, 1);
                result.insert(parts.first(), value);
            }

            return result;
        }
    }

    return QVariantMap();
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    QStringList args = app.arguments();
    if (args.length() < 5) {
      qDebug() << "usage: " << args.at(0) << " <host> <domain> <user> <password>";
      return 0;
    }

    QString host = QUrl::fromUserInput(args.at(1)).host();
    QString domain = args.at(2);
    QVariantMap authInfo = openConnectHelper(host, domain, args.at(3), args.at(4));
    if (authInfo.isEmpty()) {
        qDebug() << Q_FUNC_INFO << "unable to obtain auth info";
        return -1;
    }

    OpenConnectProvider provider;
    provider.setHost(host);
    provider.setDomain(domain);
    provider.setName("testvpn");
    provider.setCookie(authInfo.value("COOKIE").toString());
    provider.setServerCert(authInfo.value("FINGERPRINT").toString());
    provider.setNoCertCheck(true);

    VpnManager manager;
    QDBusObjectPath path = manager.create(provider);
    if (path.path().isEmpty() || path.path().isNull()) {
        qDebug() << Q_FUNC_INFO << "unable to connect provider";
        return -1;
    }

    qDebug() << Q_FUNC_INFO << "created vpn service at path: " << path.path();
    forever {
        app.processEvents();
        // need to wait for the connection to show up
        VpnConnection *connection = manager.connection(path);
        if (connection) {
            connection->connect();
            qDebug() << Q_FUNC_INFO << "connected vpn service at path: " << path.path();
            break;
        }
    }

    return 0;
}
