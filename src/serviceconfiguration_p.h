#ifndef SERVICECONFIGURATION_P_H
#define SERVICECONFIGURATION_P_H

#include "service.h"
#include "service_p.h"

class ConfigurationIPV4Data : public IPV4Data
{
public:
    ConfigurationIPV4Data(Service *service)
        : IPV4Data(service)
    {
    }

    virtual void apply();
    virtual void loadFromConfigData(const QString &data);

};

class ConfigurationIPV6Data : public IPV6Data
{
public:
    ConfigurationIPV6Data(Service *service)
        : IPV6Data(service)
    {
    }

    virtual void apply();
    virtual void loadFromConfigData(const QString &data);

};

class ConfigurationPrivateInterface
{
public:
    ConfigurationPrivateInterface(const QString &path, Service *q)
        : configurationFilePath(path),
          q_ptr(q)
    {
        if (path.isEmpty() || path.isNull())
            configurationFilePath = QLatin1String("/var/lib/connman/qconnman.config");
    }

    void initializeConfiguredDataObjects(ServicePrivate *d, Service *parent);

    bool saveConfiguration();
    bool setConnmanPropertyHelper(const QString &property, const QVariant &value);
    void loadConfigurationDataFromFile(const QString &path);
    void loadConfigurationDataFromService(Service *service);

    QString configurationFilePath;
    QVariantMap configurationData;
    Service * const q_ptr;

private:
    void loadConfigurationDataFromServiceHelper(QObject *object, QObject *child);

};

class ServiceConfigurationPrivate : public ServicePrivate,
                                    public ConfigurationPrivateInterface
{
public:
    ServiceConfigurationPrivate(const QString &path, Service *parent)
        : ServicePrivate(ObjectPropertyData(), parent),
          ConfigurationPrivateInterface(path, parent)
    {}

    virtual bool setConnmanProperty(const QString &property, const QVariant &value);

    QString macAddress;
    QString domain;
};

class WifiServiceConfigurationPrivate : public WifiServicePrivate,
                                        public ConfigurationPrivateInterface
{
public:
    WifiServiceConfigurationPrivate(const QString &path, Service *parent)
        : WifiServicePrivate(ObjectPropertyData(), parent),
          ConfigurationPrivateInterface(path, parent)
    {}

    virtual bool setConnmanProperty(const QString &property, const QVariant &value);

    QString macAddress;
};

#endif  // SERVICECONFIGURATION_P_H
