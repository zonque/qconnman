VPN_PRIVATE_HEADERS = \
    $${PWD}/vpnconnection_p.h \
    $${PWD}/vpnmanager_p.h

VPN_HEADERS = \
    $${PWD}/vpnproviders.h \
    $${PWD}/vpnagent.h \
    $${PWD}/vpnconnection.h \
    $${PWD}/vpnmanager.h

VPN_SOURCES = \
    $${PWD}/vpnproviders.cpp \
    $${PWD}/vpnconnection.cpp \
    $${PWD}/vpnmanager.cpp

vpn_header_files.files = $${VPN_HEADERS}
vpn_header_files.path = $${PREFIX}/include/qconnman/vpn
INSTALLS += vpn_header_files
