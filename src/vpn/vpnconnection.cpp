#include <QDebug>

#include "interfaces/vpn_connection_interface.h"

#include "qconnman_debug.h"
#include "vpnconnection_p.h"
#include "vpnconnection.h"

VpnRouteData::VpnRouteData(VpnConnection *parent)
    : QObject(parent),
      d_ptr(new VpnRouteDataPrivate)
{
}

VpnRouteData::~VpnRouteData()
{
}

int VpnRouteData::protocolFamily() const
{
    Q_D(const VpnRouteData);
    return d->protocolFamily;
}

void VpnRouteData::setProtocolFamily(int protocolFamily)
{
    Q_D(VpnRouteData);
    d->protocolFamily = protocolFamily;
}

QString VpnRouteData::network() const
{
    Q_D(const VpnRouteData);
    return d->network;
}

void VpnRouteData::setNetwork(const QString &network)
{
    Q_D(VpnRouteData);
    d->network = network;
}

QString VpnRouteData::netmask() const
{
    Q_D(const VpnRouteData);
    return d->netmask;
}

void VpnRouteData::setNetmask(const QString &netmask)
{
    Q_D(VpnRouteData);
    d->netmask = netmask;
}

QString VpnRouteData::gateway() const
{
    Q_D(const VpnRouteData);
    return d->gateway;
}

void VpnRouteData::setGateway(const QString &gateway)
{
    Q_D(VpnRouteData);
    d->gateway = gateway;
}

VpnConnectionPrivate::VpnConnectionPrivate(const QDBusObjectPath &path, VpnConnection *qq)
    : objectPath(path),
      immutable(false),
      index(-1),
      ipv4Data(0),
      ipv6Data(0),
      q_ptr(qq)
{
}

void VpnConnectionPrivate::initialize(const QVariantMap &properties)
{
    Q_Q(VpnConnection);
    ipv4Data = new IPV4Data(q);
    ipv4Data->setObjectName("IPv4");
    ipv6Data = new IPV6Data(q);
    ipv6Data->setObjectName("IPv6");

    connectionInterface =
        new NetConnmanVpnConnectionInterface("net.connman.vpn", objectPath.path(),
                                             QDBusConnection::systemBus(), q);
    if (!connectionInterface->isValid()) {
        qConnmanDebug() << "unable to connect to vpn connection at path: " << objectPath.path();
        return;
    }

    QObject::connect(connectionInterface, SIGNAL(PropertyChanged(QString,QDBusVariant)),
                                       q, SLOT(propertyChanged(QString,QDBusVariant)));

    // set the initial properties
    foreach (QString property, properties.keys())
        q->propertyChanged(property, QDBusVariant(properties.value(property)));
}

int VpnConnectionPrivate::s_vpnRouteDataMetaTypeId =
    qRegisterMetaType<VpnRouteData*>("VpnRouteData*");
int VpnConnectionPrivate::s_vpnRouteDataListMetaTypeId =
    qRegisterMetaType<QList<VpnRouteData*> >("QList<VpnRouteData*>");
VpnConnection::VpnConnection(const QDBusObjectPath &path, const QVariantMap &properties,
                             QObject *parent)
    : ConnManObject(parent),
      d_ptr(new VpnConnectionPrivate(path, this))
{
    Q_D(VpnConnection);
    d->initialize(properties);
}

VpnConnection::VpnConnection(VpnConnectionPrivate *dd, QObject *parent)
    : ConnManObject(parent),
      d_ptr(dd)
{
}

VpnConnection::~VpnConnection()
{
}

VpnConnection::VpnConnectionState VpnConnection::state() const
{
    Q_D(const VpnConnection);
    if (d->state == QLatin1String("idle"))
        return IdleState;
    else if (d->state == QLatin1String("failure"))
        return FailureState;
    else if (d->state == QLatin1String("configuration"))
        return ConfigurationState;
    else if (d->state == QLatin1String("ready"))
        return ReadyState;
    else if (d->state == QLatin1String("disconnect"))
        return DisconnectState;
    return UndefinedState;
}

QDBusObjectPath VpnConnection::objectPath() const
{
    Q_D(const VpnConnection);
    return d->objectPath;
}

QString VpnConnection::type() const
{
    Q_D(const VpnConnection);
    return d->type;
}

QString VpnConnection::domain() const
{
    Q_D(const VpnConnection);
    return d->domain;
}

QString VpnConnection::host() const
{
    Q_D(const VpnConnection);
    return d->host;
}

bool VpnConnection::isImmutable() const
{
    Q_D(const VpnConnection);
    return d->immutable;
}

int VpnConnection::index() const
{
    Q_D(const VpnConnection);
    return d->index;
}

IPV4Data *VpnConnection::ipv4() const
{
    Q_D(const VpnConnection);
    return d->ipv4Data;
}

IPV6Data *VpnConnection::ipv6() const
{
    Q_D(const VpnConnection);
    return d->ipv6Data;
}

QStringList VpnConnection::nameservers() const
{
    Q_D(const VpnConnection);
    return d->nameservers;
}

QList<VpnRouteData*> VpnConnection::userRoutes() const
{
    Q_D(const VpnConnection);
    return d->userRoutes;
}

QList<VpnRouteData*> VpnConnection::serverRoutes() const
{
    Q_D(const VpnConnection);
    return d->serverRoutes;
}

void VpnConnection::connect()
{
    Q_D(VpnConnection);
    QDBusPendingReply<> reply = d->connectionInterface->Connect();
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << Q_FUNC_INFO << reply.error();
    } else {
        qConnmanDebug() << "connected vpn connection(" << d->objectPath.path() << ")";
    }
}

void VpnConnection::disconnect()
{
    Q_D(VpnConnection);
    QDBusPendingReply<> reply = d->connectionInterface->Disconnect();
    reply.waitForFinished();
    if (reply.isError()) {
        qConnmanDebug() << Q_FUNC_INFO << reply.error();
    } else {
        qConnmanDebug() << "disconnected vpn connection(" << d->objectPath.path() << ")";
    }
}

QString VpnConnection::stateInternal() const
{
    Q_D(const VpnConnection);
    return d->state;
}

void VpnConnection::setStateInternal(const QString &state)
{
    Q_D(VpnConnection);
    if (d->state != state) {
        d->state = state;
        Q_EMIT dataChanged();
    }
}

void VpnConnection::setTypeInternal(const QString &type)
{
    Q_D(VpnConnection);
    d->type = type;
    Q_EMIT dataChanged();
}

void VpnConnection::setDomainInternal(const QString &domain)
{
    Q_D(VpnConnection);
    d->domain = domain;
    Q_EMIT dataChanged();
}

void VpnConnection::setHostInternal(const QString &host)
{
    Q_D(VpnConnection);
    d->host = host;
    Q_EMIT dataChanged();
}

void VpnConnection::setImmutableInternal(bool immutable)
{
    Q_D(VpnConnection);
    d->immutable = immutable;
    Q_EMIT dataChanged();
}

void VpnConnection::setIndexInternal(int index)
{
    Q_D(VpnConnection);
    d->index = index;
    Q_EMIT dataChanged();
}

void VpnConnection::setNameserversInternal(const QStringList &nameservers)
{
    Q_D(VpnConnection);
    d->nameservers = nameservers;
    Q_EMIT dataChanged();
}

OpenConnectData::OpenConnectData(OpenConnectVpnConnection *parent)
    : QObject(parent),
      d_ptr(new OpenConnectDataPrivate)
{
}

OpenConnectData::~OpenConnectData()
{
}

QString OpenConnectData::cookie() const
{
    Q_D(const OpenConnectData);
    return d->cookie;
}

void OpenConnectData::setCookieInternal(const QString &cookie)
{
    Q_D(OpenConnectData);
    d->cookie = cookie;
}

QString OpenConnectData::serverCert() const
{
    Q_D(const OpenConnectData);
    return d->serverCert;
}

void OpenConnectData::setServerCertInternal(const QString &serverCert)
{
    Q_D(OpenConnectData);
    d->serverCert = serverCert;
}

QString OpenConnectData::caCert() const
{
    Q_D(const OpenConnectData);
    return d->caCert;
}

void OpenConnectData::setCaCertInternal(const QString &caCert)
{
    Q_D(OpenConnectData);
    d->caCert = caCert;
}

QString OpenConnectData::clientCert() const
{
    Q_D(const OpenConnectData);
    return d->clientCert;
}

void OpenConnectData::setClientCertInternal(const QString &clientCert)
{
    Q_D(OpenConnectData);
    d->clientCert = clientCert;
}

QString OpenConnectData::vpnHost() const
{
    Q_D(const OpenConnectData);
    return d->vpnHost;
}

void OpenConnectData::setVpnHostInternal(const QString &vpnHost)
{
    Q_D(OpenConnectData);
    d->vpnHost = vpnHost;
}

bool OpenConnectData::noCertCheck() const
{
    Q_D(const OpenConnectData);
    return d->noCertCheck;
}

void OpenConnectData::setNoCertCheckInternal(bool certCheck)
{
    Q_UNUSED(certCheck)
    Q_D(OpenConnectData);
    d->noCertCheck = true;
}


OpenConnectVpnConnectionPrivate::OpenConnectVpnConnectionPrivate(const QDBusObjectPath &path,
                                                                 VpnConnection *qq)
    : VpnConnectionPrivate(path, qq),
      data(0)
{
}

int OpenConnectVpnConnectionPrivate::s_openConnectDataMetaTypeId =
    qRegisterMetaType<OpenConnectData*>("OpenConnectData*");
OpenConnectVpnConnection::OpenConnectVpnConnection(const QDBusObjectPath &path,
                                                   const QVariantMap &properties,
                                                   QObject *parent)
    : VpnConnection(new OpenConnectVpnConnectionPrivate(path, this), parent)
{
    Q_D(OpenConnectVpnConnection);
    d->data = new OpenConnectData(this);
    d->data->setObjectName("OpenConnect");
    d->initialize(properties);
}

OpenConnectData *OpenConnectVpnConnection::data() const
{
    Q_D(const OpenConnectVpnConnection);
    return d->data;
}

QDebug operator<<(QDebug dbg, const VpnConnection *connection)
{
    if (!connection)
        return dbg << "invalid VpnConnection";

    dbg.nospace() << "[ " << connection->objectPath().path().toLatin1().constData() << " ]" << endl;

    // first properties
    const QMetaObject *mo = connection->metaObject();
    int propertyOffset = ConnManObject::staticMetaObject.propertyOffset();
    int propertyCount = ConnManObject::staticMetaObject.propertyOffset() + mo->propertyCount();
    for (int i = propertyOffset; i < propertyCount; ++i) {
        QMetaProperty mp = mo->property(i);
        QVariant data = mp.read(connection);

        if (data.isValid() && !data.isNull()) {
            if (data.canConvert<QList<VpnRouteData*> >()) {
                dbg.nospace() << "\t" << mp.name() << " = " << data.value<QList<VpnRouteData*> >();
            } else if (data.canConvert<QObject*>()) {
                QObject *object = data.value<QObject*>();
                if (!object) {
                    dbg.nospace() << "\t" << mp.name() << " = {}" << endl;
                    continue;
                }

                dbg.nospace() << "\t" << qobject_cast<ConfigurableObject*>(object);
            } else {
                dbg.nospace() << "\t" << mp.name() << " = " << data.toString().toLatin1().constData() << endl;
            }
        }
    }

    return dbg.space();
}

QDebug operator<<(QDebug dbg, const QList<VpnRouteData*> &routeData)
{
    return dbg << "ROUTE DATA: " << routeData.size() << endl;
}

#include "moc_vpnconnection.cpp"
