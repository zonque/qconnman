#ifndef VPNAGENT_H
#define VPNAGENT_H

#include <QObject>

class VpnAgentPrivate;
class VpnAgent : public QObject
{
public:
    explicit VpnAgent(QObject *parent = 0);
    ~VpnAgent();

protected Q_SLOTS:
    /**
     * This method gets called to indicate that the agent request failed before
     * a reply was returned.
     */
    virtual void Cancel() = 0;

    /**
     * This method gets called when the service daemon unregisters the agent.
     * An agent can use it to do cleanup tasks. There is no need to unregister
     * the agent, because when this method gets called it has already been
     * unregistered.
     */
    virtual void Release() = 0;

    /**
     * This method gets called when an error has to be reported to the user.
     * A special return value can be used to trigger a retry of the failed
     * transaction.
     *
     * Possible Errors: net.connman.vpn.Agent.Error.Retry
     */
    virtual void ReportError(const QDBusObjectPath &service, const QString &error) = 0;

    /**
     * This method gets called when trying to connect to a service and some
     * extra input is required. For example a password or username.
     *
     * The return value should be a dictionary where the keys are the field
     * names and the values are the actual fields. Alternatively an error
     * indicating that the request got canceled can be returned.
     *
     * Most common return field names are "Username" and of course "Password".
     *
     * The dictionary arguments contains field names with their input parameters.
     *
     * Possible Errors: net.connman.vpn.Agent.Error.Canceled
     */
    virtual QVariantMap RequestInput(const QDBusObjectPath &service, const QVariantMap &fields) = 0;

private:
    Q_DISABLE_COPY(VpnAgent)

};

#endif
