#ifndef MANAGER_P_H
#define MANAGER_P_H

#include <QMetaType>
#include <QDBusObjectPath>

#include "manager.h"

class ManagerNode
{
public:
    ManagerNode();
    ManagerNode(Technology *technology, ManagerNode *parent = 0);
    ManagerNode(Service *service, ManagerNode *parent = 0);
    ~ManagerNode();

    bool isTechnology() const;
    bool isService() const;
    bool isRoot() const;
    QDBusObjectPath path() const;

    template<typename T>
    inline T object() const { return qobject_cast<T>(m_object); }

    ManagerNode *parent();
    ManagerNode *child(int index);
    void appendChild(ManagerNode *node);
    int childCount() const;
    int childNumber() const;
    bool removeChildren(int position, int count);

private:
    ManagerNode *m_parent;
    QList<ManagerNode*> m_children;
    QPointer<QObject> m_object;
    bool m_technology;

};
Q_DECLARE_METATYPE(ManagerNode*)

class NetConnmanManagerInterface;
class ManagerPrivate
{
public:
    ManagerPrivate()
        : managerInterface(0),
          connmanWatcher(0),
          state(Manager::Offline),
          offlineMode(false),
          sessionMode(false),
          ignoreHiddenNetworks(false),
          root(0)
    {
        if (s_roleNames.isEmpty()) {
            s_roleNames.insert(Manager::TechnologyRole, "technology");
            s_roleNames.insert(Manager::ServiceRole, "service");
            s_roleNames.insert(Qt::DisplayRole, "name");
            s_roleNames.insert(Manager::IconRole, "icon");
        }

        if (s_stateLookup.isEmpty()) {
            s_stateLookup.insert("offline", Manager::Offline);
            s_stateLookup.insert("idle", Manager::Idle);
            s_stateLookup.insert("ready", Manager::Ready);
            s_stateLookup.insert("online", Manager::Online);
        }
    }

    Technology *technologyForType(const QString &type);
    ManagerNode *nodeForIndex(const QModelIndex &index) const;
    ManagerNode *nodeForPath(const QDBusObjectPath &path, ManagerNode *parent) const;
    ManagerNode *nodeForTechnologyType(const QString &type);

    static QHash<int, QByteArray> s_roleNames;
    static QHash<QString, Manager::State> s_stateLookup;
    static int s_objectPropertyDataMetaTypeId;
    static int s_objectPropertyListDataMetaTypeId;
    static int s_serviceMetaTypeId;
    static int s_technologyMetaTypeId;

    NetConnmanManagerInterface *managerInterface;
    QDBusServiceWatcher *connmanWatcher;

    Manager::State state;
    bool offlineMode;
    bool sessionMode;
    bool ignoreHiddenNetworks;

    QHash<QDBusObjectPath, Agent*> agents;
    QList<Technology*> technologies;
    QHash<QDBusObjectPath, Service*> services;
    QHash<QDBusObjectPath, QString> providers;
    ManagerNode *root;

};

#endif
