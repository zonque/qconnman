#ifndef CONFIGURATIONFORMAT_H
#define CONFIGURATIONFORMAT_H

#include <QSettings>

namespace QConnman
{
    extern QSettings::Format ConfigurationFormat;
}

#endif // CONFIGURATIONFORMAT_H
