#ifndef SERVICECONFIGURATION_H
#define SERVICECONFIGURATION_H

#include "service.h"

class ServiceConfigurationPrivate;
class ServiceConfiguration : public Service
{
    Q_OBJECT
    Q_PROPERTY(QString MAC READ macAddress WRITE setMacAddressInternal)
    Q_PROPERTY(QString Domain READ domain WRITE setDomainInternal)
public:
    explicit ServiceConfiguration(const QString &name, const QString &path = QString(),
                                  QObject *parent = 0);
    explicit ServiceConfiguration(Service *service, const QString &path = QString(),
                                  QObject *parent = 0);
    ~ServiceConfiguration();

    QString macAddress() const;
    void setMacAddress(const QString &macAddress);

    QString domain() const;
    void setDomain(const QString &domain);

public Q_SLOTS:
    bool save();

private:
    Q_DISABLE_COPY(ServiceConfiguration)
    Q_DECLARE_PRIVATE(ServiceConfiguration)

    void setMacAddressInternal(const QString &macAddress);
    void setDomainInternal(const QString &domain);

};
Q_DECLARE_METATYPE(ServiceConfiguration*)

class WifiServiceConfigurationPrivate;
class WifiServiceConfiguration : public WifiService
{
    Q_OBJECT
    Q_PROPERTY(QString MAC READ macAddress WRITE setMacAddressInternal)
    Q_PROPERTY(QString Name READ name WRITE setNameInternal)
public:
    explicit WifiServiceConfiguration(const QString &name, const QString &path = QString(),
                                      QObject *parent = 0);
    explicit WifiServiceConfiguration(Service *service, const QString &path = QString(),
                                      QObject *parent = 0);
    ~WifiServiceConfiguration();

    QString macAddress() const;
    void setMacAddress(const QString &macAddress);

    QString name() const;
    void setName(const QString &name);

public Q_SLOTS:
    bool save();

private:
    Q_DISABLE_COPY(WifiServiceConfiguration)
    Q_DECLARE_PRIVATE(WifiServiceConfiguration)

    void setMacAddressInternal(const QString &macAddress);
    void setNameInternal(const QString &name);

};
Q_DECLARE_METATYPE(WifiServiceConfiguration*)

#endif
