#include <QSettings>
#include <QTextStream>
#include <QStringList>

#include "configurationformat.h"

namespace QConnman {

bool readConnManFile(QIODevice &device, QSettings::SettingsMap &map)
{
    // This doesn't decode reserved characters
    QTextStream inStream(&device);
    inStream.setCodec("UTF-8");

    QString currentGroup = "General";
    while (!inStream.atEnd()) {
        QString line = inStream.readLine();
        if (line.isEmpty())
            continue;
        if (line.startsWith('[')) {
            if (!line.endsWith(']'))
                continue;
            currentGroup = line.remove(0, 1).remove(line.length()-2, 1);
        } else {
            QStringList keyValue = line.split('=');
            if (keyValue.length() > 0) {
                QString fullKey = QString("%1/%2").arg(currentGroup).arg(keyValue.at(0).trimmed());
                if (keyValue.length() == 1) {
                    map.insert(fullKey, QVariant());
                } else if (keyValue.length() == 2) {
                    QString data = keyValue.at(1).trimmed();
                    if (data.contains(","))
                        map.insert(fullKey, data.split(","));
                    else
                        map.insert(fullKey, data);
                }
            }
        }
    }

    return true;
}

bool writeConnManFile(QIODevice &device, const QSettings::SettingsMap &map)
{
    // This doesn't encode reserved characters.
    QMap<QString, QStringList> groupList;
    foreach (QString fullKey, map.keys()) {
        QString group;
        if (fullKey.lastIndexOf("/") == -1)
            group = "General";
        else
            group = fullKey.mid(0, fullKey.lastIndexOf("/"));
        QStringList keyList = groupList.value(group, QStringList());

        QString valueString;
        QVariant value = map.value(fullKey);
        if (value.userType() == QVariant::StringList)
            valueString = value.toStringList().join(",");
        else
            valueString = value.toString();

        keyList << QString("%1 = %2")
                   .arg(fullKey.mid(fullKey.lastIndexOf('/')+1))
                   .arg(valueString);
        groupList.insert(group, keyList);
    }

    QTextStream outStream(&device);
    outStream.setCodec("UTF-8");
    foreach (QString group, groupList.keys()) {
        outStream << '[' << group.toLatin1() << ']' << endl;
        QStringList keyValueList = groupList.value(group);
        foreach (QString keyValue, keyValueList) {
            outStream << keyValue.toLatin1() << endl;
        }
    }

    return true;
}

QSettings::Format ConfigurationFormat =
        QSettings::registerFormat("", readConnManFile, writeConnManFile, Qt::CaseSensitive);


} // namespace QConnman
