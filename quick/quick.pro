DEPTH = ..
include($${DEPTH}/qconnman.pri)

TEMPLATE = lib
TARGET = $$qtLibraryTarget(qconnmanplugin)
CONFIG += plugin
QT += qml quick

INCLUDEPATH += $${QCONNMAN_INCLUDEPATH}
LIBS += -L$${DEPTH}/lib $${QCONNMAN_LIBS}

SOURCES = \
    plugin.cpp

OTHER_FILES += \
    qmldir

MODULENAME = DevonIT/QConnman
isEmpty(IMPORT_ROOT) {
    equals(QT_MAJOR_VERSION, 4): IMPORT_ROOT = $$[QT_INSTALL_IMPORTS]
    equals(QT_MAJOR_VERSION, 5): IMPORT_ROOT = $$[QT_INSTALL_QML]
}

TARGETPATH = $$IMPORT_ROOT/$$MODULENAME
target.path = $$TARGETPATH
qmldir.files += qmldir
qmldir.path = $$TARGETPATH
INSTALLS += target qmldir
